<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('alunos', function (Blueprint $table) {
            $table->date('data_hora_cadastro');
            $table->increments('matricula');
            $table->string('mat_final');  
            $table->string('nome');
            $table->string('foto');                
            $table->string('mat_educacenso');            
            $table->string('ident_inep');
            $table->string('nis');
            $table->string('data_nasc');
            $table->string('naturalidade');
            $table->string('uf');
            $table->string('rg');
            $table->string('org');
            $table->string('uf_rg');
            $table->string('data_exp');
            $table->string('sexo');
            $table->string('raca');
            $table->string('nacionalidade');
            $table->string('doc_estrangeiro');
            $table->string('tel_fixo');
            $table->string('pai');
            $table->string('pai_profissao');
            $table->string('pai_celular');
            $table->string('mae');
            $table->string('mae_profissao');
            $table->string('mae_celular');
            $table->string('responsavel');
            $table->string('responsavel_profissao');
            $table->string('responsavel_celular');
            $table->string('logadouro');
            $table->string('num');
            $table->string('complemento');
            $table->string('municipio');
            $table->string('localizacao');
            $table->string('cep');
            $table->string('bairro');           
            $table->string('modelo_cert');
            $table->string('ant_n_termo');
            $table->string('ant_folha');
            $table->string('ant_livro');
            $table->string('ant_data_emissao');
            $table->string('ant_nome_cartorio');
            $table->string('ant_mun_cart');
            $table->string('ant_uf_cart');
            $table->string('nov_n_mat');
            $table->string('tipo_certidao');
            $table->string('situacao_pais');
            $table->string('nova_estrutura');
            $table->string('vida_escolar');
            $table->string('dificuldade_fala');
            $table->string('deficiencia');
            $table->string('difi_locomacao');
            $table->string('desempenho_ed');
            $table->string('trans_global');
            $table->string('altas_hab');  
            $table->string('bolsa_fam');
            $table->string('escolarização');
            $table->string('transp_escolar');
            $table->string('tip_veic_transp');
            $table->string('respon_transp');
            $table->string('veiculo_aq');
            $table->string('font_fina');
            $table->string('modalidade');
            $table->string('niv_ens');
            $table->string('ano_serie_etapa');
            $table->string('turma');
            $table->string('turno');
            $table->string('unid_ens');
            $table->string('dre');
            $table->string('sit_mat');
            $table->date('data_mat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alunos');
    }
}
