<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('professores', function (Blueprint $table) {
            $table->increments('mat_prof');
            $table->string('mat_sedum');
            $table->string('mat_final');
            $table->string('nome');
            $table->string('data_nasc');
            $table->string('sexo');
            $table->string('etnia');
            $table->string('pai');
            $table->string('mae');
            $table->string('logadouro');
            $table->string('num');
            $table->string('complemento');
            $table->string('bairro');
            $table->string('municipio');
            $table->string('estado');
            $table->string('cep');
            $table->string('email');
            $table->string('tel_residencial');
            $table->string('tel_comercial');
            $table->string('celular');
            $table->string('nacionalidade');
            $table->string('naturalidade');
            $table->string('estado_civ');
            $table->string('n_filhos');
            $table->string('tipo_sang');
            $table->string('rg');
            $table->string('uf_rg');
            $table->string('org');
            $table->string('data_exp');
            $table->string('cpf');            
            $table->string('data_admissao');
            $table->string('titulo');
            $table->string('zona');
            $table->string('sessao');
            $table->string('data_titulo');
            $table->timestamps();          
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('professores');
        
    }
}
