<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(App\Professores::class, function (Faker\Generator $faker) {
    return [ 
            'mat_sedum' => $faker->randomNumber($nbDigits = 8, $strict = true),            
            'mat_final' => $faker->randomNumber($nbDigits = 8, $strict = true), 
            'nome' => $faker->name,
            'data_nasc' => $faker->date($format = 'd-m-Y', $max = 'now'),
            'sexo' => 'Masculino',
            'etnia' => 'Não declarada',
            'pai'=> $faker->name($gender = 'male'),
            'mae' => $faker->name($gender = 'female'),
            'logadouro' => $faker->streetName,
            'num' => $faker->buildingNumber,
            'complemento' => $faker->secondaryAddress,
            'bairro' => $faker->city,
            'municipio' => $faker->city,
            'estado' => 'Sergipe',
            'cep' => $faker->postcode,
            'email' => $faker ->freeEmail,
            'tel_residencial' => $faker->phoneNumber,
            'tel_comercial' => $faker->phoneNumber,
            'celular' => $faker->phoneNumber,
            'nacionalidade' => 'Brasileiro', 
            'naturalidade' => $faker->state,
            'estado_civ' => 'Solteiro (a)',           
            'n_filhos' => 'Não Possui',
            'tipo_sang' => 'A+',
            'rg' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'uf_rg' => $faker->stateAbbr,
            'org' => 'SSP',
            'data_exp' => $faker->date($format = 'd-m-Y', $max = 'now'),
            'cpf' => $faker->randomNumber($nbDigits = 8, $strict = true),            
            'data_admissao' => $faker->date($format = 'd-m-Y', $max = 'now'),
            'titulo'=> $faker->randomNumber($nbDigits = 8, $strict = true),
            'zona' => $faker->randomNumber($nbDigits = 3, $strict = true),
            'sessao' => $faker->randomNumber($nbDigits = 2, $strict = true).'º',
            'data_titulo' => $faker->date($format = 'd-m-Y', $max = 'now')
    ];
});


/* $factory->define(App\Alunos::class, function (Faker\Generator $faker) {
    return [   
            'nome' => $faker->name,
            'mat_final' => $faker->randomNumber($nbDigits = 8, $strict = true),             
            'mat_educacenso' => $faker->randomNumber($nbDigits = 8, $strict = true),          
            'ident_inep' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'nis' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'data_nasc' => $faker->date($format = 'd-m-Y', $max = 'now'),
            'naturalidade' => $faker->state,
            'uf' => $faker->stateAbbr,
            'rg' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'org' => 'SSP',
            'uf_rg' => $faker->stateAbbr,
            'data_exp' => $faker->date($format = 'd-m-Y', $max = 'now'),
            'sexo' => 'Masculino',
            'raca' => 'Não declarada',
            'nacionalidade' => 'Brasileiro', 
            'doc_estrangeiro' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'tel_fixo' => $faker->phoneNumber,
            'pai' => $faker->name($gender = 'male'),
            'pai_profissao' => $faker->jobTitle,
            'pai_celular' => $faker->phoneNumber,
            'mae' => $faker->name($gender = 'female'),
            'mae_profissao' => $faker->jobTitle,
            'mae_celular' => $faker->phoneNumber,
            'responsavel' => $faker->name($gender ='male'),
            'responsavel_profissao' => $faker->jobTitle,
            'responsavel_celular' => $faker->phoneNumber ,
            'logadouro' => $faker->streetName,
            'num' => $faker->buildingNumber,
            'complemento' => $faker->secondaryAddress,
            'municipio' => $faker->city,
            'localizacao' => 'Urbana',
            'cep' => $faker->postcode,
            'bairro' => $faker->citySuffix,
            'modelo_cert' => 'Antiga',
            'ant_n_termo' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'ant_folha' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'ant_livro' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'ant_data_emissao' => $faker->date($format = 'd-m-Y', $max = 'now'),
            'ant_nome_cartorio' => $faker->company,
            'ant_mun_cart' => $faker->randomNumber($nbDigits = 8, $strict = true),
            'ant_uf_cart' => $faker->stateAbbr,
            'nov_n_mat' => $faker->randomNumber($nbDigits = 8, $strict = true),'situacao_pais' => 'Nova',
            'nova_estrutura' => 'Mãe',
            'vida_escolar' => 'Sim-p',
            'dificuldade_fala' => 'Não',
            'deficiencia' => 'Não',
            'difi_locomacao' => 'Não',
            'desempenho_ed' => 'Não',
            'trans_global' => 'Não',
            'altas_hab' => 'Não'  ,
            'tipo_certidao' => 'Nascimento',
            'bolsa_fam'  => 'Não',
            'escolarização' => 'Não',
            'transp_escolar' => 'Sim',
            'tip_veic_transp' => 'Micro',
            'respon_transp' => 'Estadual',
            'veiculo_aq' => '100',
            'font_fina' => $faker->company,
            'modalidade' => 'Educação Regular',
            'niv_ens' => 'Ensino Fundamental (1º ao 3º)',
            'ano_serie_etapa' => '9º ano',
            'turma' => '2016',
            'turno' => 'Noite',
            'unid_ens' => 'Dom Luciano',
            'dre' => 'DRE 09',
            'sit_mat' => 'Renovada',
            'data_mat' => $faker->date($format = 'd-m-Y', $max = 'now')
    ];
}); */