<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professores extends Model
{
  protected $fillable = [
            'mat_prof',
            'mat_sedum',
            'mat_final',
            'nome',
            'data_nasc',
            'sexo',
            'etnia',
            'pai',
            'mae',
            'logadouro',
            'num',
            'complemento',
            'bairro',
            'municipio',
            'estado',
            'cep',
            'email',
            'tel_residencial',
            'tel_comercial',
            'celular',
            'nacionalidade',
            'naturalidade',
            'estado_civ',
            'n_filhos',
            'tipo_sang',
            'rg',
            'uf_rg',
            'org',
            'data_exp',
            'cpf',            
            'data_admissao',
            'titulo',
            'zona',
            'sessao',
            'data_titulo'
  ];
}
