<?php
namespace App\Http\Controllers;

use App\Aluno;
use JWTAuth;
use Illuminate\Http\Request;

class AlunosController extends Controller
{
 
    public function getAlunos()
    {
        $alunos = Aluno::all();
        $response = [
            'alunos' => $alunos
        ];
        return response()->json($response, 200);
    }

    public function postAluno(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();
        $aluno = new aluno();
        $aluno = $aluno -> create($request->all());
        $aluno->save();
        return $aluno;
    }
}
